<!DOCTYPE html>
<html lang="fr" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <title>EpiWeather</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/EpiWeather.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyA6jRGPBGDu-H1wCmQ_iM7KpUycS1IjnBE&amp;language=us-US"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery-cookie.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jeoquery.min.js"></script>
    <script src="js/EpiWeather.js" async></script>
</head>
<body>
<div class="container" id="top-container">
    <div class="jumbotron">
        <h1>EpiWeather</h1>
        <h3>A weather checker by nguye_g</h3>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-offset-2 col-lg-8">
            <div class="input-group input-group-lg">
                <input type="text" class="form-control" id="searchcity" placeholder="Find your city..."/>
                <span class="input-group-btn">
                    <button class="btn btn-primary" id="searchbutton">Get weather !</button>
                </span>
            </div>
        </div>
    </div>
    <br/>
    <div class="row" id="weatherLoader">
        <div class="col-lg-8 col-lg-offset-2">
            <h3>
                <span class="label label-info">
                    <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                    <i>&nbsp&nbspLoading weather data...</i>
                </span>
            </h3>
        </div>
    </div>
    <div class="container well" id="weatherContainer">
        <div class="row">
            <div class="col-lg-8 col-centered">
                <div class="map-container" id="gMap"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-centered" id="mapLegend">
                <img src="img/city-marker.png" alt="Smiley face" height="40" width="22">
                <span class="badge">Target city&nbsp</span>
                <img src="img/station-marker.png" alt="Smiley face" height="40" width="22">
                <span class="badge">Nearest weather station (<span class="stationDist"></span> km)</span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-centered" id="weatherData">
                <h2><strong>Weather in <i><span id="cityName"></span></i> area</strong><br/><span class="badge">updated <span id="reportPeriod"></span> ago</span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-centered">
                <h2><strong><span id="weatherTemp"></span> C°</strong></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-centered">
                <h3><strong><span class="label label-primary" id="weatherCondition"></span></strong></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-centered">
                <h3><strong><span class="label label-info" id="weatherClouds"></span></strong></h3>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-4 col-centered">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <td><strong>Weather station</strong></td>
                        <td><span id="weatherStation"></span></td>
                    </tr>
                    <tr>
                        <td><strong>Report timestamp</strong></td>
                        <td><span id="weatherTimestamp"></span></td>
                    </tr>
                    <tr>
                        <td><strong>Observation METAR code</strong></td>
                        <td><span id="weatherMETAR"></span></td>
                    </tr>
                    <tr>
                        <td><strong>Relative humidity</strong></td>
                        <td><span id="weatherHumidity"></span> %</td>
                    </tr>
                    <tr>
                        <td><strong>Atmospheric pressure</strong></td>
                        <td><span id="weatherPressure"></span> hPa</td>
                    </tr>
                    <tr>
                        <td><strong>Wind direction</strong></td>
                        <td><span id="weatherWindDirection"></span> °</td>
                    </tr>
                    <tr>
                        <td><strong>Wind speed</strong></td>
                        <td><span id="weatherWindSpeed"></span> Km/h</td>
                    </tr>
                    </tbody>
                </table>
                <button type="button" class="btn btn-info" id="btnAddToFavorite">Add to favorite</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-centered well" id="favoriteContainer">
            <h2><span class="label label-warning">
                Favorites
                <span class="glyphicon glyphicon-star"></span>
            </span></h2>
            <table class="table table-hover">
                <tbody id="favoriteTable">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
