var     EpiWeather = {
    city: null,
    weather: null,
    map: null,
    map_city: null,
    map_station: null
};

var     EpiWeatherRes = {
    station_icon: {
        url: 'img/station-marker.png',
        size: new google.maps.Size(22, 40),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 40)
    }
};

$('#weatherLoader').hide();
$('#weatherContainer').hide();
$('#searchcity').jeoCityAutoComplete({callback: loadCity});
$('#btnAddToFavorite').on('click', FavoriteAdd);
FavoriteLoadAll();

function    displayError(errorContent) {
    var oldAlert = $('#errorAlert');
    if (oldAlert)
        oldAlert.remove();

    var alertDiv = document.createElement('div');
    $(alertDiv).addClass('alert alert-danger alert-dismissible');
    alertDiv.role = 'alert';
    alertDiv.id = 'errorAlert';
    $(alertDiv).append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(alertDiv).append(errorContent);

    $('#weatherLoader').hide();
    $('#top-container').append(alertDiv);
}

function    loadCity(city) {
    EpiWeather.city = city;
    $('#searchbutton').on('click', WeatherRequest);
}

function    GMapInit() {
    var gmapProp = {
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };

    EpiWeather.map = new google.maps.Map(document.getElementById("gMap"), gmapProp);

    google.maps.event.addDomListener(window, 'resize', function() {
        EpiWeather.map.setCenter(new google.maps.LatLng(EpiWeather.city.lat, EpiWeather.city.lng));
    });
}

function    GMapUpdate() {
    var lat = EpiWeather.city.lat;
    var lng = EpiWeather.city.lng;

    if (EpiWeather.map == null)
        GMapInit();

    EpiWeather.map.setCenter(new google.maps.LatLng(lat, lng));
    GMapUpdateCity();
    GMapUpdateStation();
}

function    GMapUpdateCity() {
    if (EpiWeather.city == null)
        return;

    if (EpiWeather.map_city != null)
    {
        EpiWeather.map_city.setMap(null);
        EpiWeather.map_city = null;
    }

    EpiWeather.map_city = new google.maps.Marker({
        map: EpiWeather.map,
        title: EpiWeather.city.name,
        position:   new google.maps.LatLng(EpiWeather.city.lat, EpiWeather.city.lng),
        animation:  google.maps.Animation.DROP
    });
}

function    GMapUpdateStation() {
    if (EpiWeather.weather == null)
        return;

    if (EpiWeather.map_station != null)
    {
        EpiWeather.map_station.setMap(null);
        EpiWeather.map_station = null;
    }

    EpiWeather.map_station = new google.maps.Marker({
        map: EpiWeather.map,
        icon: EpiWeatherRes.station_icon,
        title: EpiWeather.weather.stationName + ' Weather Station',
        position:   new google.maps.LatLng(EpiWeather.weather.lat, EpiWeather.weather.lng),
        animation:  google.maps.Animation.DROP
    });
}

function    WeatherRequest() {
    $('#weatherContainer').hide();
    $('#weatherLoader').show();
    jeoquery.getGeoNames('findNearByWeather', {lat: EpiWeather.city.lat, lng: EpiWeather.city.lng}, WeatherUpdate);
}

function    WeatherUpdate(data) {
    if (!data.hasOwnProperty('weatherObservation'))
    {
        displayError('<strong>Error !</strong> We don\'t have any weather data in <i>' + EpiWeather.city.name + '</i> area');
        return;
    }

    EpiWeather.weather = data.weatherObservation;

    var stationDist = ComputeDistance(EpiWeather.city.lat, EpiWeather.city.lng,
        EpiWeather.weather.lat, EpiWeather.weather.lng);
    $('.stationDist').html(Math.round(stationDist / 10) / 100);

    var reportPeriod = Math.round((new Date() - new Date(EpiWeather.weather.datetime)) / 60000);

    var reportPeriodH = Math.round(reportPeriod / 60);
    switch (reportPeriodH)
    {
        case 0:
            break;
        case 1:
            reportPeriodH += ' hour ';
            break;
        default:
            reportPeriodH += ' hours ';
    }

    var reportPeriodM = reportPeriod % 60;
    switch (reportPeriodM)
    {
        case 0:
            break;
        case 1:
            reportPeriodM += ' minute';
            break;
        default:
            reportPeriodM += ' minutes';
    }
    $('#reportPeriod').empty().append(reportPeriodH).append(reportPeriodM);

    GMapUpdate();

    $('#cityName').html(EpiWeather.city.name);
    $('#weatherTemp').html(EpiWeather.weather.temperature);
    if (EpiWeather.weather.weatherCondition == 'n/a')
        $('#weatherCondition').html('No rainfall');
    else
        $('#weatherCondition').html(EpiWeather.weather.weatherCondition);
    if (EpiWeather.weather.clouds == 'n/a')
        $('#weatherClouds').html('No cloud');
    else
        $('#weatherClouds').html(EpiWeather.weather.clouds);
    $('#weatherStation').html(EpiWeather.weather.stationName);
    $('#weatherTimestamp').html(EpiWeather.weather.datetime);
    $('#weatherMETAR').html(EpiWeather.weather.observation);
    $('#weatherHumidity').html(EpiWeather.weather.humidity);
    $('#weatherPressure').html(EpiWeather.weather.hectoPascAltimeter);
    $('#weatherWindDirection').html(EpiWeather.weather.windDirection);
    $('#weatherWindSpeed').html(EpiWeather.weather.windSpeed);

    $('#weatherLoader').hide();
    $('#weatherContainer').show();
}

/**
 * @return {number}
 */
function    ComputeDistance(lat1, lng1, lat2, lng2) {
    var R = 6371e3; // Meters

    var φ1 = lat1 * Math.PI / 180;
    var φ2 = lat2 * Math.PI / 180;
    var Δφ = (lat2 - lat1) * Math.PI / 180;
    var Δλ = (lng2 - lng1) * Math.PI / 180;

    var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
        Math.cos(φ1) * Math.cos(φ2) *
        Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return R * c;
}

function    FavoriteAdd() {
    $.cookie('EWF_' + btoa( EpiWeather.city.lng +  EpiWeather.city.lat).substr(0, 15), JSON.stringify({
            fname: EpiWeather.city.name,
            flat: EpiWeather.city.lat,
            flng: EpiWeather.city.lng
        }), {expires: 365});
    FavoriteLoadAll();
}

function    FavoriteLoadAll() {
    var fTable = $('#favoriteTable');
    fTable.empty();
    var cookies = document.cookie ? document.cookie.split('; ') : [];
    for (var i = 0, l = cookies.length; i < l; i++) {
        var name = decodeURIComponent(cookies[i].split('=').shift());
        if (!name.match('^EWF_'))
            continue;

        var cookie = JSON.parse($.cookie(name));
        var btnWeatherName = 'bW' + name;
        var btnDeleteName = 'bD' + name;

        fTable.append(  '<tr><td><strong>' + cookie.fname  + '</strong></td>' +
                        '<td>' + cookie.flat  + '° ,' + cookie.flng + '°</td>' +
                        '<td><div class="btn-group" role="group">' +
                        '<button type="button" class="btn btn-success" id="' + btnWeatherName + '">Get weather !</button>' +
                        '<button type="button" class="btn btn-danger" id="' + btnDeleteName + '">Delete</button>' +
                        '</div></td>' +
                        '</tr>');

        $('#' + btnWeatherName).on('click', FavoriteGetWeather);
        $('#' + btnDeleteName).on('click', FavoriteDel);
    }
}

function    FavoriteGetWeather() {
    var cookie = JSON.parse($.cookie(this.id.substr(2)));

    if (EpiWeather.city == null) {
        EpiWeather.city = {
            name: cookie.fname,
            lat: cookie.flat,
            lng: cookie.flng
        };
    } else {
        EpiWeather.city.name = cookie.fname;
        EpiWeather.city.lat = cookie.flat;
        EpiWeather.city.lng = cookie.flng;
    }

    WeatherRequest();
}

function    FavoriteDel() {
    $.removeCookie(this.id.substr(2));
    FavoriteLoadAll();
}
